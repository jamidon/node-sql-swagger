// Unpublished work © 2018, The Narwhal Group
// All Rights Reserved
//
//    THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF THE NARWHAL GROUP
//    The copyright notice above does not evidence any
//    actual or intended publication of such source code.

/*jslint node: true, nomen: true, white: true, indent: 2 */

'use strict';

const async = require('async');
const fs = require('fs');
const info = require('./package.json');
const mustache = require('mustache');
const path = require('path');
const sql = require('sql');
const util = require('util');

const supportedDialects = {
  mysql: 1,
  pg: 1,
  mssql: 1
};

/**
 * @param {Object} options
 * @param {String} options.dialect Either "mysql", "pg" or "mssql"
 * @param {String} options.dsn The DSN to use to connect to the database
 * @param {String} options.schema The name of the schema/database to extract from
 * @param {String|Number} [options.databaseDefinitionsFile] Filename to write swagger definitions to, or the number 1 to write to stdout
 * @param {String|Number} [options.databaseReferencesFile] Filename to write swagger references to, or the number 1 to write to stdout
 * @param {String|Number} [options.databaseCodeFile] Filename to write database code to, or the number 1 to write to stdout
 * @param {String|Number} [options.databaseCodeHeaderTemplateFile] Filename containing database template header code
 * @param {String|Number} [options.databaseCodeTemplateFile] Filename containing database template code
 * @param {String|Number} [options.pathDefinitionsFile] Filename to write swagger definitions to, or the number 1 to write to stdout
 * @param {String|Number} [options.pathDefinitionsTemplateFile] Filename containing the path template
 * @param {String|Number} [options.pathReferencesTemplateFile] Filename containing the path template
 * @param {Boolean} [options.camelize] Convert underscored names to camel case ("foo_bar" -> "fooBar")
 * @param {String} [options.mode] The permission mode of the output file, defaults to 0644
 * @param {String} [options.encoding] The encoding of the output file, defaults to "utf8"
 * @param {String} [options.prepend] String to prepend to the beginning of the generated output
 * @param {String} [options.append] String to append to the end of the generated output
 * @param {String} [options.excludeRegex] Comma separated list of regular expressions for tables to skip
 * @param {String} [options.includeSchema] Include schema in definition
 * @param {function} [options.log] Logging function
 * @param {Function} [callback]
 */
module.exports = function(options, callback) {
  options = options || {};
  // console.log(util.inspect(options));
  if (!options.dsn) {
    callback && callback(new Error('options.dsn is required'));
    return;
  }
  if (!options.dialect) {
    let match = /^(mysql|postgres|mssql)/i.exec(options.dsn);
    if (!match) {
      callback && callback(new Error('options.dialect is required'));
      return;
    }
    options.dialect = match[1].toLowerCase();
    if (options.dialect === 'postgres') {
      options.dialect = 'pg';
    }
  }

  options.dialect = options.dialect.toLowerCase();

  if (!supportedDialects[options.dialect]) {
    callback && callback(new Error('options.dialect must be either "mysql", "pg" or "mssql"'));
    return;
  }

  options.eol = options.eol || '\n';
  options.encoding = options.encoding || 'utf8';
  options.mode = options.mode || 0o644;
  options.databaseCodeHeaderTemplateFile =
    options.databaseCodeHeaderTemplateFile || path.join(__dirname, `templates/${options.dialect}/swaggerDb-header.mustache`);
  options.databaseCodeTemplateFile =
    options.databaseCodeTemplateFile || path.join(__dirname, `templates/${options.dialect}/swaggerDb.mustache`);
  options.databaseDefinitionsTemplateFile =
    options.databaseDefinitionsTemplateFile || path.join(__dirname, 'templates/database-definitions.mustache');
  options.databaseReferencesTemplateFile =
    options.databaseReferencesTemplateFile || path.join(__dirname, 'templates/database-references.mustache');
  options.pathDefinitionsTemplateFile =
    options.pathDefinitionsTemplateFile || path.join(__dirname, 'templates/path-definitions.mustache');
  options.pathReferencesTemplateFile =
    options.pathReferencesTemplateFile || path.join(__dirname, 'templates/path-references.mustache');

  var log = options.log || function() {},
      fd = null,
      client = null,
      tableList = null,
      stats = {
	start: new Date(),
	end: null,
	elapsed: null,
	written: 0,
	buffer: '',
	tables: {}
      },
      db = require(options.dialect),
      columns,
      tables,
      table_constraints,
      constraint_column_usage;

  function camelize(name) {
    return name.replace(/_(.)/g, function(all, c) {
      return c.toUpperCase();
    });
  }

  function tryCamelize(name) {
    return !options.camelize ? name : camelize(name);
  }

  function pathize(name) {
    return name.replace(/_(.)/g, function(all, c) {
      return c + '/';
    });
  }

  function runQuery(query, callback) {
    //query = query.toQuery();
    log('debug', 'QUERY: ' + query.text + ' :: ' + util.inspect(query.values));
    switch (options.dialect) {
    case 'mysql':
      client.query(query.text, query.values, callback);
      break;
    case 'pg':
      // console.log('QUERY: ' + query.text + ' :: ' + util.inspect(query.values));
      client.query(query.text, query.values, function(err, result) {
	callback(err, result && result.rows);
      });
      break;
    case 'mssql':
      let req = client.request();

      query.values.map(function(value, index) {
	req.input(index + 1, value);
      });

      req.query(query.text, callback);
      break;
    }
  }

  function write() {
    // console.log(util.inspect(arguments));
    let string = [].slice.call(arguments, 0, -1)
	.map(function(obj) { return (obj || '').toString(); })
	.filter(function(message) { return !!message; })
	.join(options.eol) + options.eol,
	callback = arguments[arguments.length - 1],
	buffer = Buffer.from(string, options.encoding);

    if (fd) {
      fs.write(fd, buffer, 0, buffer.length, null, function(err, written) {
	stats.bytesWritten += (written || 0);
	callback(err);
      });
    } else {
      stats.buffer += buffer.toString();
      stats.bytesWritten += buffer.length;
      callback();
    }
  }

  function getListOfTables(callback) {
    let query = tables
	.select(
          tables.name.as('name'),
          tables.schema.as('schema'),
          tables.type.as('type')
        )
	.from(tables);

    switch (options.dialect) {
    case 'mysql':
      query = query.where(tables.schema.equals(options.database));
      break;
    case 'pg':
      query = query
	.where(tables.schema.equals(options.schema))
	.and(tables.catalog.equals(options.database));
      break;
    case 'mssql':
      query = query
	.where(tables.schema.equals(options.schema || 'dbo'))
	.and(tables.catalog.equals(options.database))
	.and(tables.type.equals('BASE TABLE'))
	.and(tables.name.notEquals('sysdiagrams')); //disconsider views
      break;
    }

    query = query.order(tables.name);

    runQuery(query.toQuery(), function(err, rows) {
      if (err) {
	callback(err);
	return;
      }
      let listOfTables = {};
      rows.forEach(r => { /*console.log(util.inspect(r));*/ listOfTables[r.name] = r; });
      callback(null, listOfTables);
    });
  }

  function getListOfPrimaryKeyColumns(tableName, callback) {
    let query = table_constraints
        .select(
          columns.name.as('name'),
          columns.type.as('type')
        )
        .from(
          table_constraints
            .join(constraint_column_usage)
            .on(table_constraints.constraintSchema.equals(constraint_column_usage.constraintSchema)
                .and(
                  table_constraints.constraintName.equals(constraint_column_usage.constraintName))
               )
            .join(columns)
            .on(constraint_column_usage.constraintSchema.equals(columns.tableSchema)
                .and(
                  table_constraints.tableName.equals(columns.tableName))
                .and(
                  constraint_column_usage.columnName.equals(columns.name))
               )
        )
        .where(table_constraints.tableName.equals(tableName), table_constraints.type.equals('PRIMARY KEY'))
        .toQuery();

    // console.log(query);

    runQuery(query, function(err, results) {
      if (err) {
	callback(err);
	return;
      }

      // console.log(results);
      callback(null, results);
    });
  }

  function getListOfColumns(tableName, callback) {
    getListOfPrimaryKeyColumns(tableName, function(err, pkColumns) {
      // console.log('table: %s, keys: %s', tableName, pkColumns.map(r => r.name).join(','));
      let query = columns
	  .select(
	    columns.name.as('name'),
	    columns.isNullable.as('nullable'),
	    columns.defaultValue.as('defaultValue'),
	    columns.charLength.as('charLength'),
            columns.precision.as('precision'),
            columns.tableName.as('tableName'),
	    columns.type.as('type')
	  )
	  .from(columns)
	  .where(columns.tableName.equals(tableName));

      switch (options.dialect) {
      case 'mysql':
        query = query.and(columns.tableSchema.equals(options.database));
        break;
      case 'pg':
        query = query
	  .and(columns.tableSchema.equals(options.schema))
	  .and(columns.tableCatalog.equals(options.database));
        break;
      case 'mssql':
        query = query
	  .and(columns.tableSchema.equals(options.schema || 'dbo'))
	  .and(columns.tableCatalog.equals(options.database));
        break;
      }

      query = query.order(columns.ordinalPosition);

      runQuery(query.toQuery(), function(err, results) {
        if (err) {
	  callback(err);
	  return;
        }

        results = results.map(function(col) {
	  switch (col.type) {
	  case 'character varying':
	    col.type = 'varchar';
	    break;
	  case 'character':
	    col.type = 'char';
	    break;
	  case 'integer':
	    col.type = 'int';
	    break;
	  }

	  col.nullable = col.nullable === 'YES';
	  col.maxLength = parseInt(col.maxLength) || null;
          col.isPrimaryKey = pkColumns.findIndex(pk => pk.name === col.name) !== -1;

	  return col;
        });

        callback(null, results);
      });
    });
  }

  switch (options.dialect) {
  case 'mysql':
    client = db.createConnection(options.dsn);
    options.database = options.database || client.config.database;
    sql.setDialect('mysql');
    columns = sql.define({
      name: 'COLUMNS',
      schema: 'information_schema',
      columns: [
	{ name: 'TABLE_SCHEMA', property: 'tableSchema' },
	{ name: 'TABLE_NAME', property: 'tableName' },
	{ name: 'COLUMN_NAME', property: 'name' },
	{ name: 'ORDINAL_POSITION', property: 'ordinalPosition' },
	{ name: 'DATA_TYPE', property: 'type' },
	{ name: 'CHARACTER_MAXIMUM_LENGTH', property: 'charLength' },
	{ name: 'COLUMN_DEFAULT', property: 'defaultValue' },
	{ name: 'IS_NULLABLE', property: 'isNullable' }
      ]
    });
    tables = sql.define({
      name: 'TABLES',
      schema: 'information_schema',
      columns: [
	{ name: 'TABLE_NAME', property: 'name' },
	{ name: 'TABLE_SCHEMA', property: 'schema' }
      ]
    });
    break;
  case 'pg':
    sql.setDialect('postgres');
    client = new db.Client(options.dsn);
    options.database = options.database || client.database;
    options.schema = options.schema || 'public';
    columns = sql.define({
      name: 'columns',
      schema: 'information_schema',
      columns: [
	{ name: 'table_catalog', property: 'tableCatalog' },
	{ name: 'table_schema', property: 'tableSchema' },
	{ name: 'table_name', property: 'tableName' },
	{ name: 'column_name', property: 'name' },
	{ name: 'ordinal_position', property: 'ordinalPosition' },
	{ name: 'data_type', property: 'type' },
	{ name: 'numeric_precision', property: 'precision' },
	{ name: 'character_maximum_length', property: 'charLength' },
	{ name: 'column_default', property: 'defaultValue' },
	{ name: 'is_nullable', property: 'isNullable' }
      ]
    });
    tables = sql.define({
      name: 'tables',
      schema: 'information_schema',
      columns: [
	{ name: 'table_catalog', property: 'catalog' },
	{ name: 'table_name', property: 'name' },
	{ name: 'table_schema', property: 'schema' },
	{ name: 'table_type', property: 'type' }
      ]
    });
    table_constraints = sql.define({
      name: 'table_constraints',
      schema: 'information_schema',
      columns: [
	{ name: 'table_catalog', property: 'tableCatalog' },
	{ name: 'table_schema', property: 'tableSchema' },
	{ name: 'table_name', property: 'tableName' },
	{ name: 'constraint_catalog', property: 'constraintCatalog' },
	{ name: 'constraint_schema', property: 'constraintSchema' },
	{ name: 'constraint_name', property: 'constraintName' },
	{ name: 'constraint_type', property: 'type' }
      ]
    });
    constraint_column_usage = sql.define({
      name: 'constraint_column_usage',
      schema: 'information_schema',
      columns: [
	{ name: 'table_catalog', property: 'tableCatalog' },
	{ name: 'table_schema', property: 'tableSchema' },
	{ name: 'table_name', property: 'tableName' },
	{ name: 'column_name', property: 'columnName' },
	{ name: 'constraint_catalog', property: 'constraintCatalog' },
	{ name: 'constraint_schema', property: 'constraintSchema' },
	{ name: 'constraint_name', property: 'constraintName' }
      ]
    });
    break;
  case 'mssql':
    sql.setDialect('mssql');
    //Extract information from mssql dsn to options, since the mssql module do not understand the dsn format
    let mssqlDsn = options.dsn;
    if (mssqlDsn.slice(-1) === ';') {
      mssqlDsn = mssqlDsn.substring(0, mssqlDsn.length - 1);
    }
    try {
      mssqlDsn = JSON.parse("{\"" +
			    mssqlDsn.replace('mssql://', '')
			    .replace(/=/g, '\":\"')
			    .replace(/;/g, '\",\"') +
			    "\"}"
			   );
    } catch (e) {
      callback(e);
      return;
    }

    client = new db.Connection(mssqlDsn);
    options.database = options.database || mssqlDsn.database;
    columns = sql.define({
      name: 'columns',
      schema: options.database + '].[information_schema',
      columns: [
	{ name: 'table_schema', property: 'tableSchema' },
	{ name: 'table_name', property: 'tableName' },
	{ name: 'table_catalog', property: 'tableCatalog' },
	{ name: 'column_name', property: 'name' },
	{ name: 'ordinal_position', property: 'ordinalPosition' },
	{ name: 'data_type', property: 'type' },
	{ name: 'character_maximum_length', property: 'charLength' },
	{ name: 'numeric_precision', property: 'precision' },
	{ name: 'column_default', property: 'defaultValue' },
	{ name: 'is_nullable', property: 'isNullable' }
      ]
    });
    tables = sql.define({
      name: 'tables',
      schema: options.database + '].[information_schema',
      columns: [
	{ name: 'table_name', property: 'name' },
	{ name: 'table_schema', property: 'schema' },
	{ name: 'table_catalog', property: 'catalog' },
	{ name: 'table_type', property: 'type' }
      ]
    });
    break;
  }

  if (!options.database) {
    callback(new Error('options.database is required if it is not part of the DSN'));
    return;
  }

  function fetchTables(next) {
    getListOfTables(function(err, tables) {
      tableList = tables;
      next(err);
    });
  }

  function openDatabaseDefinitionsFile(next) {
    if (typeof(options.databaseDefinitionsFile) === 'string') {
      fs.open(options.databaseDefinitionsFile, 'w', options.mode, function(err, descriptor) {
	if (!err) {
	  fd = descriptor;
	}

	next(err);
      });
    } else {
      if (options.databaseDefinitionsFile === 1) {
	fd = 1; //stdout
      }

      process.nextTick(next);
    }
  }

  function openDatabaseReferencesFile(next) {
    if (typeof(options.databaseReferencesFile) === 'string') {
      if (typeof(options.databaseReferencesFile) === 'string') {
        fs.closeSync(fd);
      }
      fs.open(options.databaseReferencesFile, 'w', options.mode, function(err, descriptor) {
	if (!err) {
	  fd = descriptor;
	}

	next(err);
      });
    } else {
      if (options.databaseReferencesFile === 1) {
	fd = 1; //stdout
      }

      process.nextTick(next);
    }
  }

  function openDatabaseCodeFile(next) {
    if (typeof(options.databaseCodeFile) === 'string') {
      if (typeof(options.databaseCodeFile) === 'string') {
        fs.closeSync(fd);
      }
      fs.open(options.databaseCodeFile, 'w', options.mode, function(err, descriptor) {
	if (!err) {
	  fd = descriptor;
	}

	next(err);
      });
    } else {
      if (options.databaseCodeFile === 1) {
	fd = 1; //stdout
      }

      process.nextTick(next);
    }
  }

  function openPathDefinitionsFile(next) {
    if (typeof(options.pathDefinitionsFile) === 'string') {
      fs.open(options.pathDefinitionsFile, 'w', options.mode, function(err, descriptor) {
	if (!err) {
	  fd = descriptor;
	}

	next(err);
      });
    } else {
      if (options.pathDefinitionsFile === 1) {
	fd = 1; //stdout
      }

      process.nextTick(next);
    }
  }

  function openPathReferencesFile(next) {
    if (typeof(options.pathReferencesFile) === 'string') {
      if (typeof(options.pathReferencesFile) === 'string') {
        fs.closeSync(fd);
      }
      fs.open(options.pathReferencesFile, 'w', options.mode, function(err, descriptor) {
	if (!err) {
	  fd = descriptor;
	}

	next(err);
      });
    } else {
      if (options.pathReferencesFile === 1) {
	fd = 1; //stdout
      }

      process.nextTick(next);
    }
  }

  function connect(next) {
    log('debug', 'Attempting connection with DSN "' + options.dsn + '"');
    client.connect(next);
  }

  function writeHead(next) {
    log('info',
	'Starting generation against ' + options.database +
	(options.schema ? '.' + options.schema : '')
       );

    let functions = [];
    if (options.prepend) {
      functions.push(function(next) {
	write(options.prepend + options.eol, next);
      });
    }

    async.series(functions, next);
  }

  function writeHeadDatabaseFile(next) {
    log('info',
	'Starting generation against ' + options.database +
	(options.schema ? '.' + options.schema : '')
       );

    let functions = [];
    functions.push(function(next) {
      const template = fs.readFileSync(options.databaseCodeHeaderTemplateFile, 'utf8');

      let args = [];
      args.push(mustache.render(template, {
        product: {
          name: info.name,
          version: info.version
        },
        copyright: {
          year: new Date().getFullYear()
        }
      }));

      args.push(function(err) {
        next(err);
      });

      write.apply(null, args);
    });

    async.series(functions, next);
  }

  const intTypes = [ 'bigint', 'smallint', 'integer', 'int' ];
  const numberTypes = [ 'numeric', 'float', 'real', 'double precision' ];
  const stringTypes = [ 'text', 'string', 'varchar' ];
  const byteTypes = [ 'bit' ];
  const binaryTypes = [ 'bytea' ];

  function getFormatFromDbType(dbt, precision) {
    if (intTypes.includes(dbt)) {
      return 'int' + precision.toString();
    } else if (numberTypes.includes(dbt)) {
      if (dbt === 'numeric' || dbt.startsWith('double')) {
        return 'double';
      }
      return 'float';
    } else if (byteTypes.includes(dbt)) {
      return 'binary';
    } else if (binaryTypes.includes(dbt)) {
      return 'byte';
    } else if (dbt.startsWith('timestamp')) {
      return 'date-time';
    } else if (dbt.startsWith('date')) {
      return 'date';
    }

    return null;
  }

  function getTypeFromDbType(column) {
    const dbt = column.type;
    if (dbt === 'boolean') {
      return 'boolean';
    } else if (intTypes.includes(dbt)) {
      return 'integer';
    } else if (numberTypes.includes(dbt)) {
      return 'number';
    } else if (stringTypes.includes(dbt) || binaryTypes.includes(dbt) || byteTypes.includes(dbt)) {
      return 'string';
    }
    // console.log('[' + column.tableName + '.' + dbt + '] is string?');
    return 'string';
  }

  function hasDefaultValue(val) {
    if (!val) {
      return false;
    }
    return typeof(val) === 'string' ?
      !(val.startsWith('nextval') ||
        val.startsWith('date') ||
        val.startsWith('time')) :
      true;
  }

  function createRequiredBlock(columns) {
    // console.log('createRequiredBlock(args, %s)', util.inspect(columns));
    const required = columns
          .filter(c => !c.nullable && !c.defaultValue);
    if (required.length) {
      return required.map(c => c.name).join(',');
    }

    return null;
  }

  // const allOfProperties = [ 'pkey', 'date_created', 'last_updated' ];
  function createAllOfRequiredBlock(columns) {
    const required = columns
          .filter(c => !c.nullable);
    if (required.length) {
      return required.map(c => c.name).join(',');
    }

    return null;
  }

  function createPropertyBlock(column) {
    let propertyBlock = {
      name: column.name,
      type: getTypeFromDbType(column)
    };
    const format = getFormatFromDbType(column.type, column.precision);
    if (format) {
      propertyBlock.format = format;
    }
    if (column.charLength) {
      propertyBlock.maxLength = column.charLength;
    }
    if (hasDefaultValue(column.defaultValue)) {
      propertyBlock.default = column.defaultValue.split('::')[0].replace(/\'/g, '');
    }

    return propertyBlock;
  }

  function createPropertiesBlock(columns) {
    const props = columns
       // .filter(c => !allOfProperties.includes(c.name));
          .filter(c => !c.isPrimaryKey || (c.isPrimaryKey && !c.defaultValue));
    if (props.length) {
      return props.map(c => createPropertyBlock(c));
    }

    return null;
  }

  function createAllOfPropertiesBlock(columns) {
    const props = columns
        // .filter(c => allOfProperties.includes(c.name));
          .filter(c => !(!c.isPrimaryKey || (c.isPrimaryKey && !c.defaultValue)));
    if (props.length) {
      return props.map(c => createPropertyBlock(c));
    }

    return null;
  }

  function createNewInstanceBlock(table, columns) {
    let newBlock = {
      newRow: tryCamelize('new_' + table.name + '_row'),
      newProperties: createPropertiesBlock(columns)
    };
    // args.push(definition + ':');
    const requiredBlock = createRequiredBlock(columns);
    if (requiredBlock) {
      newBlock.newRequired = requiredBlock;
    }

    return newBlock;
  }

  function createAllOfInstanceBlock(table, columns) {
    let allOfBlock = {
      newRow: tryCamelize('new_' + table.name + '_row')
    };
    let allOfProperties = createAllOfPropertiesBlock(columns);
    if (allOfProperties) {
      allOfBlock.allOfProperty = {
        allOfProperties: allOfProperties
      };
    }
    const requiredBlock = createAllOfRequiredBlock(columns);
    if (requiredBlock) {
      allOfBlock.allOfRequired = requiredBlock;
    }

    return allOfBlock;
  }

  const databaseDefinitions = [];
  function createInstanceBlock(table, columns) {
    const view = {
      tableNameRow: tryCamelize(table.name + '_row'),
      tableNameRows: tryCamelize(table.name + '_rows')
    };

    if (table.type === 'VIEW') {
      view.view_name = table.name;
      view.viewProperties = createPropertiesBlock(columns);
    } else {
      view.table_name = table.name;
      view.newRow = createNewInstanceBlock(table, columns);
      view.allOfRow = createAllOfInstanceBlock(table, columns);

      databaseDefinitions.push(view.newRow.newRow);
    }
    databaseDefinitions.push(view.tableNameRow);
    databaseDefinitions.push(view.tableNameRows);

    return view;
  }
  function createDatabaseDefinitions(next) {
    const template = fs.readFileSync(options.databaseDefinitionsTemplateFile, 'utf8');

    function createDatabaseDefinitionsFromTable(table, tableName, next) {
      if (options.excludeRegex && !options.excludeRegex.every(re => tableName.match(re) === null)) {
          log('info', 'skipping ' + tableName);
          next();
      } else {
        // console.log('createDatabaseDefinitionsFromTable(%s, %s)', util.inspect(table), key);
        let start = Date.now();
        log('info', 'Starting ' + options.database + '.' + (options.schema ? options.schema + '.' : '') + tableName + '...');
        stats.tables[tableName] = {
	  columns: []
        };
        getListOfColumns(tableName, function(err, columnData) {
	  if (err) {
	    next(err);
	    return;
	  }

	  log('debug', '  Found ' + columnData.length + ' columns');
          // console.log('pkeys: %s', columnData.filter(c => c.isPrimaryKey).map(c => c.name).join(','));

          stats.tables[tableName] = {
	    columns: JSON.parse(JSON.stringify(columnData))
          };

	  let args = [],
	      fullName = (options.schema || options.database) + '.' + tableName;

          const view = createInstanceBlock(table, columnData);
          // console.log(util.inspect(view));
          args.push(mustache.render(template, view));

          args.push(function(err) {
            if (!err) {
              log('debug', '  ...finished! (' + (Date.now() - start) + 'ms)');
            }

            next(err);
          });

          write.apply(null, args);
        });
      }
    }
    async.eachOfSeries(tableList, createDatabaseDefinitionsFromTable, next);
  }
  function createDatabaseReferences(next) {
    const definitions = [];
    function createDatabaseReferencesFromDefinitions(definition, next) {
      definitions.push({
        definition: definition,
        databaseDefinitionsFile: path.basename(options.databaseDefinitionsFile)
      });
      process.nextTick(next);
    }
    async.eachSeries(databaseDefinitions, createDatabaseReferencesFromDefinitions, function() {
      const template = fs.readFileSync(options.databaseReferencesTemplateFile, 'utf8');
      const args = [];
      args.push(mustache.render(template, {
        definitions: definitions
      }));
      args.push(function() {
        next();
      });

      write.apply(null, args);
    });
  }
  function getTagName(table) {
    const tableName = (table.type !== 'VIEW' || !table.name.startsWith('view_') ?
                       table.name :
                       table.name.slice(5, table.name.length));

    const n = tableName.indexOf('_');
    if (n === -1) {
      return tableName;
    }
    return tableName.slice(0, n);
  }

  const stdColumns = ['pkey', 'name', 'site_id', 'mesowest_identifier'];
  function createGetParameters(columns) {
    const parameters = [];
    const stdParameters = [];
    columns.forEach(function(c) {
      const parameter = {
        name: c.name,
        description: 'exact match to column value',
          in: 'query',
        required: false,
        type: getTypeFromDbType(c),
        format: getFormatFromDbType(c.type, c.precision)
      };
      parameters.push(parameter);

      if (stdColumns.includes(c.name)) {
        parameters.push({
          name: c.name + 's',
          description:  'list of comma separated ' + c.name + 's',
            in: 'query',
          required: false,
          type: 'string'
        });
      }
    });
    const tColumns = columns.filter(c => c.type.startsWith('timestamp'));
    if (tColumns.length > 0) {
      parameters.push({
        ref: '#/parameters/interval',
        required: false
      });
      parameters.push({
        ref: '#/parameters/duration',
        required: false
      });
      parameters.push({
        name: 'timeColumn',
        description: 'time column to use for interval or duration query',
          in: 'query',
        required: false,
        type: 'string',
        default: tColumns[0].name,
        enum: '[ ' + tColumns.map(c => c.name).join(',') + ' ]'
      });
    }
    parameters.concat(stdParameters);
    parameters.push({
      ref: '#/parameters/formatEnum',
      required: false
    });
    parameters.push({
      ref: '#/parameters/orderBy',
      required: false
    });
    parameters.push({
      ref: '#/parameters/limitBy',
      required: false
    });

    return parameters;
  }
  const pathDefinitions = [];
  function createPaths(args, template, table, columns) {
    const view = {
      getEndPoint: table.name.replace(/_/g,'-'),
      insertEndPoint: table.type === 'VIEW' ? undefined : table.name.replace(/_/g,'-') + '-add',
      deleteEndPoint: table.type === 'VIEW' ? undefined : table.name.replace(/_/g,'-') + '-remove',
      updateEndPoint: table.type === 'VIEW' ||
        columns.findIndex(c => c.name === 'last_updated') === -1 ? undefined : table.name.replace(/_/g,'-') + '-update',
      tags: getTagName(table),
      table_name: table.name,
      tableName: tryCamelize(table.name),
      newTableName: tryCamelize('new_' + table.name),
      getParameters: createGetParameters(columns)
    };
    args.push(mustache.render(template, view));
    [view.getEndPoint, view.insertEndPoint, view.updateEndPoint, view.deleteEndPoint].forEach(p => {
      if (p) {
        pathDefinitions.push(p);
      }
    });
  }
  function createPathDefinitions(next) {
    const template = fs.readFileSync(options.pathDefinitionsTemplateFile, 'utf8');
    function createPathDefinitionsFromTable(table, tableName, next) {
      if (options.excludeRegex && !options.excludeRegex.every(re => tableName.match(re) === null)) {
          log('info', 'skipping ' + tableName);
          next();
      } else {
        // console.log('createPathDefinitionsFromTable(%s, %s)', util.inspect(table), key);
        let start = Date.now();
        log('info', 'Starting ' + options.database + '.' + (options.schema ? options.schema + '.' : '') + tableName + '...');

        const columns = stats.tables[tableName].columns;

	let args = [],
	    fullName = (options.schema || options.database) + '.' + tableName;

        createPaths(args, template, table, columns);
        args.push(' ');

        args.push(function(err) {
          if (!err) {
            log('debug', '  ...finished! (' + (Date.now() - start) + 'ms)');
          }

          next(err);
        });

        write.apply(null, args);
      }
    }
    async.eachOfSeries(tableList, createPathDefinitionsFromTable, next);
  }

  function createPathReferences(next) {
    const paths = [];
    function createPathReferencesFromDefinitions(definition, next) {
      paths.push({
        endPointPath: '/' + definition.replace(/-/g, '/').replace(/_/g,'/'),
        pathDefinitionsFile: path.basename(options.pathDefinitionsFile),
        endPoint: definition
      });
      process.nextTick(next);
    }
    async.eachSeries(pathDefinitions, createPathReferencesFromDefinitions, function() {
      const template = fs.readFileSync(options.pathReferencesTemplateFile, 'utf8');
      const args = [];
      args.push(mustache.render(template, {
        endPoints: paths
      }));
      args.push(function() {
        next();
      });

      write.apply(null, args);
    });
  }

  function generateDatabaseCodeForTable(args, template, table, columns, tableName) {
    const view = {
      module: path.basename(options.databaseCodeFile,'.js'),
      tableName: tryCamelize(tableName),
      fullTablename: (options.schema || options.database) + '.' + tableName,
      table_name: tableName
    };

    // do we have any points?
    const extraSelect = columns
          .filter(c => c.type.startsWith('USER-DEFINED'))
          .map(c => '\'ST_AsText(' + c.name + ') as ' + c.name + '\'')
          .join(',');
    if (extraSelect) {
      view.extra_select = ',' + extraSelect;
    }
    if (table.type !== 'VIEW') {
      view.insert = true;
      view.delete = true;
      if (stats.tables[tableName].columns.findIndex(c => c.name === 'last_updated') !== -1) {
        view.update = true;
        view.primary_keys = columns.filter(c => c.isPrimaryKey).map(c => { return { primary_key: c.name }; });
        for (let n = 0; n < view.primary_keys.length-1; ++n) {
          view.primary_keys[n].comma = ',';
        }
        // console.log(util.inspect(view.primary_keys));
      }
    }
    args.push(mustache.render(template, view));
  }

  function generateDatabaseCode(next) {
    const codeTemplate = fs.readFileSync(options.databaseCodeTemplateFile, 'utf8');

    function generateDatabaseCodeFromTable(table, tableName, next) {
      if (options.excludeRegex && !options.excludeRegex.every(re => tableName.match(re) === null)) {
        log('info', 'skipping ' + tableName);
        next();
      } else {
        let start = Date.now();
        log('info', 'Starting ' + options.database + '.' + (options.schema ? options.schema + '.' : '') + tableName + '...');

        const columns = stats.tables[tableName].columns;

        const args = [];
        generateDatabaseCodeForTable(args, codeTemplate, table, columns, tableName);

        args.push(function(err) {
          if (!err) {
            log('debug', '  ...finished! (' + (Date.now() - start) + 'ms)');
          }

          next(err);
        });

        write.apply(null, args);
      }
    }
    async.eachOfSeries(tableList, generateDatabaseCodeFromTable, next);
  }

  function writeTail(next) {
    tail();

    function tail(err) {
      if (err) {
	next(err);
	return;
      }

      if (options.append) {
	write(options.append, next);
      } else {
	process.nextTick(next);
      }
    }
  }

  async.series([
    openDatabaseDefinitionsFile,
    connect,
    writeHead,
    fetchTables,
    createDatabaseDefinitions,
    writeTail,
    openDatabaseReferencesFile,
    writeHead,
    createDatabaseReferences,
    writeTail,
    openDatabaseCodeFile,
    writeHeadDatabaseFile,
    generateDatabaseCode,
    writeTail,
    openPathDefinitionsFile,
    writeHead,
    createPathDefinitions,
    writeTail,
    openPathReferencesFile,
    writeHead,
    createPathReferences,
    writeTail
  ], function(appError) {
    if (appError) {
      log('error', appError);
    }

    stats.end = new Date();
    stats.elapsed = stats.end.getTime() - stats.start.getTime();
    if (fd && fd !== 1) {
      fs.close(fd, function(err) {
	if (err) {
	  log('warn', 'Error closing file: ' + err);
	}

	log('info',
	    '\nAll done! Wrote ' + stats.bytesWritten + ' bytes to ' +
	    (options.outputFile || 'stdout') + ' in ' + stats.elapsed + 'ms',
	    'yay!'
	   );

	callback(appError, stats);
      });
    } else {
      callback(appError, stats);
    }
  });
};
